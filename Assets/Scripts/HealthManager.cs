using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HealthManager : MonoBehaviour
{
    public int health = 100;
    public int takeDamage = 20;
    public Text textHealth;

    // Start is called before the first frame update
    void Start()
    {
        textHealth = textHealth.GetComponent<Text>();
        textHealth.text = "Health : " + health.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void GetDamage()
    {
        health -= 20;
        if (health < 20)
        {
            SceneManager.LoadScene("main_game");
        }

        UpdateHealthBar();
    }
    void UpdateHealthBar()
    {
        textHealth.text = "Health : " + health.ToString();
    }
}
