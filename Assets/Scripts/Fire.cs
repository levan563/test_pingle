using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public ParticleSystem damageParticle;
    [SerializeField] private GameObject healthManagerObject;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnTriggerStay(Collider other)
    {
        healthManagerObject.GetComponent<HealthManager>().GetDamage();
    }
}
