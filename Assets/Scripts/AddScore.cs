using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AddScore : MonoBehaviour
{
    public GameObject bonusParticle;

    [SerializeField] private GameObject textScore;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GameObject bonusParticleGameObject = Instantiate(bonusParticle, gameObject.transform);
            GetComponent<Renderer>().enabled = false;
            Destroy(gameObject, 1);
            textScore.GetComponent<Score>().AddScore();
        }

    }
}
