using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Text textScore;
    private int scoreCount= 0;
    // Start is called before the first frame update
    void Start()
    {
        textScore = textScore.GetComponent<Text>();
        textScore.text = "Score : " + scoreCount.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void AddScore()
    {
        scoreCount++;
        textScore.text = "Score : " + scoreCount.ToString();
    }

}
