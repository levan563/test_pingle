using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public Transform Player;
    public GameObject damageParticle;
    [SerializeField] private GameObject healthManagerObject;

    NavMeshAgent EnemyNavMeshAgent;

    // Start is called before the first frame update
    void Start()
    {
        EnemyNavMeshAgent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        AttackPlayer();
    }
    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            GameObject damageParticleGameObject = Instantiate(damageParticle, gameObject.transform);
            Destroy(gameObject, 0.5f);
            healthManagerObject.GetComponent<HealthManager>().GetDamage();
        }

    }
    void AttackPlayer()
    {
        float distance = Vector3.Distance(transform.position, Player.position);
        EnemyNavMeshAgent.SetDestination(Player.position);
    }
}
